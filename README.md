# Newicon Incoming Call Popup Window / Notification
A simple chrome app, it uses a singleton hidden window that connects to the push service and listens for incoming calls.
When a new call comes through it pops up a new window with the caller information and some quick links.

This could be quite a handy stand alone app.  Also it is persistet so you should not need Chrome running for the app to be running.

	 
## Screenshot
![screenshot](https://bytebucket.org/newicon/chrome-call-app/raw/c2aa468f3f138daed04947fc35738b2eee754e43/assets/screenshot.png)

