/**
 * Launches a hidden background window. 
 * This is a hidden singelton form that connects to the usher server
 * and subscribes to the call modules push messages.  
 * This then launches additional popups based on incomain calls etc
 * @see http://developer.chrome.com/apps/app.runtime.html
 * @see http://developer.chrome.com/apps/app.window.html
 */
chrome.app.runtime.onLaunched.addListener(function() {
    chrome.app.window.create('main.html', {
            id: "callwindow",
            frame: "none",
            hidden: true,
            resizable: false
    });
});