// On Creation the singleton view passes in the uuid by
// placing it on the callpop's window object
// window.uuid
// window channel (the push message channel object)
var CallPop = angular.module('CallPop', []);
CallPop.service('CallData', function(){
    var data = {};
    data.data = window.callData;
    return data;
});
CallPop.controller('CallPopController', function ($scope, $timeout, $http, CallData) {
    
    $scope.data = CallData.data;
    
    // Build the date object
    $scope.date = {};
    $scope.ringing = true;
    $scope.voicemail = false;
    $scope.answeredBy = 'Steve';
    $scope.answeredByData = {};

    $scope.ring = 0;
    console.log(window.callData, 'window.callData');
    console.log(CallData.data, 'CallData.data');

    $http({method: 'GET', url: "http://hub.newicon.net/call/api/call/uuid/" + window.callData.uuid})
    .success(function (data, status, headers, config) {
        console.log('data', data)
        var resp = data
        window.callData = resp;
        $scope.call = resp;
        console.log($scope.call, '$scope.call');
    });

    $scope.close = function () {
        window.close();
    }

    var ringer = function () {
        $scope.ring = ($scope.ringing == true) ? !$scope.ring : 0;
        if ($scope.ringing == true)
            $timeout(ringer, 1000);
    };
    ringer();


    // window.channel.on('answered', function(o) {
// 		var o = JSON.parse(o);
// 		if (o.uuid == window.uuid) {
// 			$http({method:'GET', url:"http://hub.newicon.net/call/api/call/uuid/"+window.uuid})
// 			.success(function(data, status, headers, config){
// 				if (data.from_contacts.length > 0)
// 					$scope.answeredByData = data.from_contacts[0]
// 			});
//
// 			console.log('answered',o);
// 			$scope.ringing = false;
// 			window.appWindow.setBounds({height:270});
// 			$scope.answeredBy = o.name;
// 			$scope.$apply();
// 		}
// 	});

    // hang up
    // window.channel.on('hangup', function(o){
//
// 	})
//
// 	// voicemail
// 	window.channel.on('voicemail', function(o) {
// 		console.log('voicemail!!!', o.uuid, window.uuid)
// 		var o = JSON.parse(o);
// 		if (o.uuid == window.uuid) {
// 			$scope.voicemail = true;
// 			$scope.$apply();
// 		}
// 	});

});

// load external images
CallPop.directive('contactImage', function () {
    return {
        restrict: 'E',
        scope: {
            url: '@'
        },
        link: function (scope, element, attr) {
            var ixhr = new XMLHttpRequest();
            console.log('scope.url', scope.url)
            ixhr.open('GET', scope.url, true);
            ixhr.responseType = 'blob';
            ixhr.onload = function (e) {
                if (this.status == 200) {
                    var blob = this.response;
                    var img = new Image();
                    img.onload = function (e) {
                        // Clean up after yourself
                        window.URL.revokeObjectURL(img.url);
                    };
                    img.src = window.URL.createObjectURL(blob);
                    img.id = attr.id;
                    // Do something with the img
                    element.replaceWith(img);
                }
            };
            ixhr.send();
        }
    };
});

CallPop.hangup = function () {
    console.log('IVE hung up, do something style wise to indicate. May want to leave window open')
};
//document.getElementById('view').src = 'http://hub.newicon.net/call/index/callinfo/uuid/'+window.uuid;

CallPop.recieveMessage = function(data){
    var injector = CallPop.getInjector();
    callDataService = injector.get('CallData');
    callDataService.recieveMessage(data);
}

CallPop.getInjector = function(){
    return angular.element(window.document).injector();
};