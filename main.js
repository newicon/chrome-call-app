// Returns a new notification ID used in the notification.
function getNotificationId() {
    var id = Math.floor(Math.random() * 9007199254740992) + 1;
    return id.toString();
}

function createNotification(data) {
    var contact = data.contact;
    chrome.notifications.create(getNotificationId(), {
        title: contact.title,
        iconUrl: 'ringing.png',
        type: 'basic',
        priority: 2,
        message: contact.body
    }, function () {});
}

/**
 * Create a new Callpop up window.
 * @param object data gets added to the window's window.callData property
 * data.uuid is an expected parameter
 * @returns {undefined}
 */
function createCallPopWindow(data) {
    if (!data.uuid)
        throw "The data parameter must have a uuid property";
    chrome.app.window.create('callpop.html', {
        frame: "none",
        type: "panel",
        id: "pop" + data.uuid,
        bounds: {
            top: 0,
            left: Math.round(screen.width - 424),
            width: 424,
            height: 135
        },
        alwaysOnTop: true,
        focused: false,
        resizable: false
    }, function (createdWindow) {
        // pass data to the window object of the created window.
        // to access the callData ppropertyy i.e. window.callData
        createdWindow.contentWindow.callData = data;
        // The initial bounds passed into the create method can be overriden with the laat height the window
        // was when it was last used.  This forces it to redraw to the initial state
        createdWindow.setBounds({height: 135});
    });
}

$(document).ready(function () {
    window.addEventListener("message", function (event) {
        console.log(event.data, 'recieved by main');
        var data = event.data;
        callPopWindow = chrome.app.window.get('pop' + data.uuid);
        console.log(callPopWindow, 'callPopWindow init');
        // maybe change to find window. 
        // if found then send through the push data
        // as it will be information about an existing call.
        // if the window with id ('pop' + uuid) is not found then create a new one
        // this would represent a new call.
        
        if (data.type == 'hangup') {
            callPopWindow = chrome.app.window.get('pop' + data.uuid);
            console.log(callPopWindow, 'callpopwindow hangup');
            callPopWindow.contentWindow.CallPop.hangup();
            // for now leave the window open if it has been hung up.  
            // But we could close it here using:
            // callPopWindow.close();
        }
        else if (data.type == 'incoming') {
            createCallPopWindow(data);
            createNotification(data);
        }
        else if (data.type = 'answered') {
            
        }
    });
});

