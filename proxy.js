/**
 * This page is snadboxed and so allowed to talk to Faye this then posts data up to its parent.
 * The main.html thus the main.js script
 */
var client = new Faye.Client('https://hub.newicon.net:8008/faye');
client.subscribe('/hub/call', function (data) {
    console.log('push message recieved:', data);
    window.top.postMessage(data, "*");
});