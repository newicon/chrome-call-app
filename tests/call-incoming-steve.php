<?php
$url = 'https://hub.newicon.net:8008/faye';
$body = array('channel' => '/hub/call', 'data' => array(
	'contact' => array(
		'body' => 'Managing Director - Newicon Ltd',
		'title' => 'Steve O\'Brien',
		'image' => 'https://www.gravatar.com/avatar/84a6778f82775b54a5f7ba38c916f68f?s=80&d=https://hub.newicon.net//assets/62d362cc/styles/images/contact.jpg&r=g',
		'url' => '/crm/index/index#/contact/10299',
	),
	'from' => '447969088447',
	'type' => "incoming",
	'uuid' => '84ab6628-ba19-11e4-85bd-a9eab5fe6629'
));
$bodyJson = json_encode($body);
$curl = curl_init($url);
curl_setopt($curl, CURLOPT_VERBOSE, false);
curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
curl_setopt($curl, CURLOPT_POSTFIELDS, $bodyJson);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
curl_setopt($curl, CURLOPT_TIMEOUT, 2000);
curl_setopt($curl, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'Content-Length: ' . strlen($bodyJson)
));

$response = curl_exec($curl);
$error = curl_error($curl);
$header_size = curl_getinfo($curl, CURLINFO_HEADER_SIZE);
$result['header'] = substr($response, 0, $header_size);
$result['body'] = substr($response, $header_size );
$result['http_code'] = curl_getinfo($curl, CURLINFO_HTTP_CODE);
$result['last_url'] = curl_getinfo($curl, CURLINFO_EFFECTIVE_URL);
if ($error != "" ) {
    $result['curl_error'] = $error;
}
echo json_encode($result);
curl_close($curl);