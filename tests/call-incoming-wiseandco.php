<?php
$url = 'https://hub.newicon.net:8008/faye';

$body = array('channel' => '/hub/call', 'data' => array(
	'contact' => array(
		'body' => "Steve South\nTreena Turner\nJennifer Cam\nDavid Walker\n",
		'title' => 'Wise & Co Chartered Accountants',
		'image' => 'http://www.gravatar.com/avatar/366b94f7553ded5075bce4768462bbb0?s=80&d=https://hub.newicon.net/assets/62d362cc/styles/images/company.jpg&r=g',
		'url' => '/hub.newicon.net/htdocs/crm/index/index#/contact/10293',
	),
	'from' => '441252711244',
	'type' => "incoming",
	'uuid' => 'a6680cb4b36e11e49843a9eab5fe6629'
));
$bodyJson = json_encode($body);
$curl = curl_init($url);
curl_setopt($curl, CURLOPT_VERBOSE, false);
curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
curl_setopt($curl, CURLOPT_POSTFIELDS, $bodyJson);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
curl_setopt($curl, CURLOPT_TIMEOUT, 2000);
curl_setopt($curl, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'Content-Length: ' . strlen($bodyJson)
));

$response = curl_exec($curl);
$error = curl_error($curl);
$header_size = curl_getinfo($curl, CURLINFO_HEADER_SIZE);
$result['header'] = substr($response, 0, $header_size);
$result['body'] = substr($response, $header_size );
$result['http_code'] = curl_getinfo($curl, CURLINFO_HTTP_CODE);
$result['last_url'] = curl_getinfo($curl, CURLINFO_EFFECTIVE_URL);
if ($error != "" ) {
    $result['curl_error'] = $error;
}
echo json_encode($result);
curl_close($curl);